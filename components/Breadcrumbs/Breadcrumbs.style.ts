import { makeStyles } from "constants/theme";

export interface StyleProps {

}

export const useStyles = makeStyles<StyleProps>()((theme, {}) => ({
    typography: {
        color: theme.palette.getContrastText(theme.palette.mode === 'dark' ? theme.palette.grey[800] : '#fff')
    }
}));

export default useStyles;
