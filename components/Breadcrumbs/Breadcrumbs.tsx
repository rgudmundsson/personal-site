import * as React from 'react';

import Link from 'next/link';
import { useRouter } from 'next/router';

import MuiBreadcrumbs from '@mui/material/Breadcrumbs';
import Typography from '@mui/material/Typography';

import NavigateNextIcon from '@mui/icons-material/NavigateNext';

import useStyles, { type StyleProps } from './Breadcrumbs.style';

interface Props extends StyleProps {}

const parseToLabel = (pathName: string) => {
    return pathName
        .split('-')
        .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
        .join(' ');
};

export const Breadcrumbs: React.FC<Props> = ({ ...styleProps }) => {
    const { classes } = useStyles(styleProps);
    const { pathname } = useRouter();

    const path = pathname
        .slice(1)
        .split('/')
        .filter((p) => p !== '');

    return (
        <MuiBreadcrumbs
            separator={<NavigateNextIcon fontSize="small" className={classes.typography} />}
            aria-label="breadcrumb"
        >
            <Typography key="home" variant="subtitle1" className={classes.typography}>
                <Link href="/">Home</Link>
            </Typography>
            {path.map((p, ind) => (
                <Typography key={p} variant="subtitle1" className={classes.typography}>
                    <Link href={'/' + path.slice(0, ind + 1).join('/')}>{parseToLabel(p)}</Link>
                </Typography>
            ))}
        </MuiBreadcrumbs>
    );
};

export default Breadcrumbs;
