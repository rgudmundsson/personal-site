import * as React from 'react';

import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import HomeIcon from '@mui/icons-material/Home';
import Switch from '@mui/material/Switch';
import { useTheme } from '@mui/material/styles';

import { useRouter } from 'next/router';
import Link from 'next/link';

import Breadcrumbs from 'components/Breadcrumbs';

import useStyles, { type StyleProps } from './MainLayout.style';

const pages = [
    {
        label: 'Demos',
        link: '/demos',
    },
    {
        label: 'About',
        link: '/about',
    },
    {
        label: 'Contact Me',
        link: '/contact-me',
    },
];

interface Props extends StyleProps {
    updateThemeMode: (mode: 'light' | 'dark') => void;
    children: React.ReactNode;
}

const MainLayout: React.FC<Props> = ({ children, updateThemeMode, ...styleProps }) => {
    const { push } = useRouter();
    const { classes } = useStyles(styleProps);
    const theme = useTheme();

    return (
        <>
            <AppBar position="static">
                <Container maxWidth="xl">
                    <Toolbar disableGutters>
                        <IconButton
                            onClick={() => {
                                push('/');
                            }}
                            size="large"
                            sx={{ mr: 2 }}
                        >
                            <HomeIcon />
                        </IconButton>
                        <Box sx={{ display: { md: 'flex', xs: 'none' }, flexGrow: 1 }}>
                            {pages.map((page) => (
                                <Typography sx={{ mr: 2 }} key={page.link}>
                                    <Link href={page.link}>{page.label}</Link>
                                </Typography>
                            ))}
                        </Box>
                        <FormGroup>
                            <FormControlLabel
                                control={
                                    <Switch
                                        checked={theme.palette.mode === 'dark'}
                                        name="dark"
                                        onChange={(e) => {
                                            const mode = e.target.checked ? 'dark' : 'light';
                                            sessionStorage.setItem('themeMode', mode);
                                            updateThemeMode(mode);
                                        }}
                                    />
                                }
                                label="Dark Mode"
                            />
                        </FormGroup>
                    </Toolbar>
                </Container>
            </AppBar>
            <main className={classes.content}>
                <div className={classes.breadcrumbs}>
                    <Breadcrumbs />
                </div>
                <div className={classes.innerContent}>{children}</div>
            </main>
        </>
    );
};

export default MainLayout;
