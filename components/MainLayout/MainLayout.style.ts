import { makeStyles } from "constants/theme";

export interface StyleProps {}

export const useStyles = makeStyles<StyleProps>()((theme) => ({
    content: {
        padding: 24,
        display: 'flex',
        flexDirection: 'column',
        backgroundColor: theme.palette.mode === 'dark'
            ? theme.palette.grey[900]
            : '#fff',
            // height: '100%',
        flexGrow: 1,
    },
    innerContent: {
        flexGrow: 1,
        backgroundColor: theme.palette.mode === 'dark'
            ? theme.palette.grey[800]
            : '#fff',
        padding: 8,
    },
    breadcrumbs: {
        marginBottom: 8,
    }
}));

export default useStyles;
