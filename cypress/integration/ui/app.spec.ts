/// <reference types="Cypress" />

describe('Navigation', () => {
    it('should navigate to the demos page', () => {
        cy.visit('http://localhost:3000');
        cy.get('a[href*="demos"]').click();
        cy.url().should('include', '/demos');
        cy.get('h6').contains('Demos');
    });
    it('should navigate to the about page', () => {
        cy.visit('http://localhost:3000');
        cy.get('a[href*="about"]').click();
        cy.url().should('include', '/about');
        cy.get('h6').contains('About');
    });
    it('should navigate to the contact me page', () => {
        cy.visit('http://localhost:3000');
        cy.get('a[href*="contact-me"]').click();
        cy.url().should('include', '/contact-me');
        cy.get('h6').contains('Contact Me');
    });
});
