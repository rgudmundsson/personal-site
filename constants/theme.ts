import type { ThemeOptions } from "@mui/material"
import { useTheme } from "@mui/material/styles";
import { cyan, blueGrey } from '@mui/material/colors';

import { createMakeAndWithStyles } from "tss-react";

export const defaultTheme: ThemeOptions = {
    palette: {
        primary: {
            main: cyan[600],
            light: cyan[400],
            dark: cyan[800]
        },
        secondary: {
            main: blueGrey[500],
            light: blueGrey[300],
            dark: blueGrey[700]
        },
        mode: 'dark',
    }
}

export const { makeStyles, withStyles } = createMakeAndWithStyles({
    useTheme,
})
