import * as React from 'react';
import '../styles/globals.css';

import type { AppProps } from 'next/app';

import { createTheme, ThemeProvider, type Theme } from '@mui/material/styles';
import { CacheProvider } from '@emotion/react';
import createCache from '@emotion/cache';

import MainLayout from 'components/MainLayout';
import { defaultTheme } from 'constants/theme';

const initialTheme = createTheme(defaultTheme);

const muiCache = createCache({
    key: 'mui',
    prepend: true,
});

export const MyApp: React.FC<AppProps> = ({ Component, pageProps }) => {
    const [theme, setTheme] = React.useState<Theme>(initialTheme);

    const updateThemeMode = React.useCallback(
        (mode: 'dark' | 'light') => {
            setTheme({
                ...theme,
                palette: {
                    ...theme.palette,
                    mode,
                },
            });
        },
        [theme]
    );

    React.useEffect(() => {
        const mode = sessionStorage.getItem('themeMode') || 'light';
        setTheme((t) => ({
            ...t,
            palette: {
                ...t.palette,
                mode: mode as 'dark' | 'light',
            },
        }));
    }, []);

    return (
        <CacheProvider value={muiCache}>
            <ThemeProvider theme={theme}>
                <MainLayout updateThemeMode={updateThemeMode}>
                    <Component {...pageProps} />
                </MainLayout>
            </ThemeProvider>
        </CacheProvider>
    );
};

export default MyApp;
